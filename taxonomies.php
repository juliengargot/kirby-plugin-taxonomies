<?php
/**
 *
 * Kirby Taxonmies plugin
 *
 * @version   0.0.1
 * @author    Julien Gargot <julien@g-u-i.me>
 * @copyright Julien Gargot <julien@g-u-i.me>
 * @link      https://github.com/julien-gargot/kirby-plugin-taxonomies
 * @license   CC BY-SA
 */

// E X T E N S I O N   R E G I S T R Y
//
$kirby->set('blueprint', 'term',         __DIR__ . DS .'blueprints'. DS . 'term.yml');
$kirby->set('blueprint', 'vocabulary',   __DIR__ . DS .'blueprints'. DS . 'vocabulary.yml');
$kirby->set('blueprint', 'fields/terms', __DIR__ . DS .'blueprints'. DS . 'fields'. DS . 'terms.yml');

// H O O K S
//

include_once( __DIR__ . DS .'hooks'. DS . 'move.php');
