# Kirby *Taxonomies* plugin

A simple taxonomies preconfigured system that allows you to create *vocabularies* and *terms* and to link pages to terms.

Plugin tries to maintain relation between pages and terms when terms are moved ~~or deleted~~.



## Requirement and Settings

There are a predefined [templates](https://getkirby.com/docs/panel/blueprints) and [field definition](https://getkirby.com/docs/panel/blueprints/global-field-definitions) that comes with the plugin.

To make everything work you have to follow few steps.

###### In the panel

You must create `vocabulary` folder at the root of your site contents that is named “Tags”.
If you want to use another name, it is possible but it will require extra settings in the next step.

###### In your blueprints

Add a field that extends the `terms` field definition into the template you want to link to your vocabulary terms:
```
tags: terms
```
or if you used another vocabulary name:
```
tags:
  extends: terms
  query:
    page: uid-from-your-vocabulary-page
```


###### In your config file

To limit resource issues when a term is moved (new URL-appendix), plugin look only for specific templates and fields to update. By default it looks for the `tags` field in the `default` template. If you are good with those value, you are done, if not, you should look at available options.



## Options

You can target a single template or an array of templates:
```
c::set('taxonomies.targets.templates', 'default');
// OR
c::set('taxonomies.targets.templates', ['article','project']);
```

You can target a single field or an array of fields:
```
c::set('taxonomies.targets.fields', tags);
// OR
c::set('taxonomies.targets.fields', ['tags','terms']);
```



## Installation

Download or clone the repository in `/site/plugins/taxonomies`.



### Known issues

- If the term is present into different vocabularies: *the term will be replaced in all fields*.

- If a target page cannot be updated: *it seems that no error is triggered*.



### TODO:

- [ ] Auto update URI when a term name is changed.
- [ ] Remove a term from targets when deleted.
- [ ] Plugin should allow Vocabulary creation from the panel.
