<?php
kirby()->hook('panel.page.move', function($page, $oldPage)
{
  $oldId = $oldPage->uid();
  $newId = $page->uid();
  if( $page->intendedTemplate() != "term" || $oldId === $newId ) return;

  // V A R S
  //
  $success = true;
  $templates = c::get('taxonomies.targets.templates', 'default');
  $fields = c::get('taxonomies.targets.fields', 'tags');
  $searchFields = is_array($fields) ? 'fields' : is_string($fields) ? 'field' : null;
  $fields = is_array($fields) ? implode("|",$fields) : is_string($fields) ? $fields : null;
  $pages = site()->index()
                 ->search($oldId, array(
                   $searchFields => $fields,
                   'words'  => true
                 ))
                 ->filterBy('intendedTemplate', 'in', $templates);

  $logs = array();

  foreach ($pages as $p) :

      if ( is_array($fields) ) :

        foreach ($fields as $field) :

          try
          {
            $oldValue = $p->{$field}()->value();
            $newValue = str_replace($oldId, $newId, $oldValue);
            $p->update(array(
              $field => $newValue
            ));
          }
          catch(Exception $e)
          {
            $success = false;
            $logs[$p->uid()][$field] = 'Error';
          }

        endforeach;

      else :

        try
        {
          $oldValue = $p->{$fields}()->value();
          $newValue = str_replace($oldId, $newId, $oldValue);
          $p->update(array(
            $fields => $newValue
          ));
        }
        catch(Exception $e)
        {
          $success = false;
          $logs[$p->uid()][$fields] = 'Error';
        }

      endif;

  endforeach;

  // D E B B U G
  // Warn user if something went wrong.
  //
  try
  {
    if (!$success) :
      panel()->alert( 'Sorry, site encountered some errors.' );
      panel()->redirect($page);
    endif;
    $page->update(array(
      'logs' => var_export($logs, true)
    ));
  }
  catch(Exception $e)
  {
  }

});
